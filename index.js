const express = require('express')
const axios = require('axios').default;
const app = express()
const url = 'https://jsonplaceholder.typicode.com'



app.get('/', function (req, res) {
    res.send('Hello World')
})

app.get(`/users`, async (req, res) => {
    try {
        const { data } = await axios.get(`${url}/users`);
        res.send(data);
    } catch (error) {
        console.error(error);
    }
})

app.get(`/users/:id`, async (req, res) => {
    try {
        const { data } = await axios.get(`${url}/users/${req.params.id}`);
        res.send(data);
    } catch (error) {
        console.error(error);
    }
})

app.get(`/users/:id/posts`, async (req, res) => {
    try {
        const { data } = await axios.get(`${url}/users/${req.params.id}/posts`);
        res.send(data);
    } catch (error) {
        console.error(error);
    }
})

app.get(`/users/:id/albums`, async (req, res) => {
    try {
        let  { data } = await axios.get(`${url}/users/${req.params.id}/albums`);
        const albums = data;
        console.log(albums)
        for (let i in albums) {
            let { data } = await axios.get(`${url}/photos?albumId=${albums[i].id}`);
            let photos = data;
            albums[i].photos = photos;
            console.log(photos)
        }
        console.log(albums)
        res.send(albums);
    } catch (error) {
        console.error(error);
    }
})

app.post(`/users`, async (req, res) => {
    try {
        const data= await axios.post(`${url}/users`, {
                name: 'Fred',
                username: 'Flintstone'
        })
        res.send(data);
    } catch (error) {
        console.error(error);
    }
})

app.patch('/users/:id', async (req, res) => {
    try {
        const { data } = await axios.patch(`${url}/users/${req.params.id}`, {
            name: 'Fred',
            username: 'Flintstone'
        })
        res.send(data);
    } catch (error) {
        console.error(error);
    }
})

app.delete(`/users/:id`, async (req, res) => {
    try {
        const {data} = await axios.delete(`${url}/users/${req.params.id}`);
        res.send("C'est bien supprimé");

    } catch (error) {
        console.error(error);
        res.send(error);
    }
})




app.listen(3000)
